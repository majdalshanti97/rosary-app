import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlAzkar extends StatefulWidget {
  @override
  _AlAzkarState createState() => _AlAzkarState();
}

class _AlAzkarState extends State<AlAzkar> {
  int _counter = 0;
  String _alZeker = "استغفر الله";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "تسبيح",
          style: TextStyle(color: Colors.black, fontSize: 25),
        ),


        actions: [
          PopupMenuButton<String>(
              offset: Offset(-30, 20),
              icon: Icon(
                Icons.more_vert,
                color: Colors.lightGreen,
              ),
              onSelected: (String selectedItem) {
                switch (selectedItem) {
                  case 'z1':
                    _changeItem("سبحان الله");
                    break;
                  case 'z2':
                    _changeItem("استغفر الله");
                    break;
                  case 'z3':
                    _changeItem("الله اكبر");
                    break;
                }
              },
              itemBuilder: (context) {
                return [
                  PopupMenuItem(
                    child: Text("سبحان الله"),
                    value: 'z1',
                  ),
                  PopupMenuItem(
                    child: Text("استغفر الله"),
                    value: 'z2',
                  ),
                  PopupMenuItem(
                    child: Text("الله اكبر "),
                    value: 'z3',
                  ),
                ];
              })
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Scaffold(
          body: Column(
            children: [
              Image.asset(
                "images/logo.png",
                width: double.infinity,
                height: 300,
              ),
              SizedBox(
                height: 90,
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                        padding: const EdgeInsets.all(10),
                        height: 50,
                        color: Colors.lightGreen,
                        child: Text(
                          _alZeker,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        )),
                  ),
                  Container(
                    color: Colors.redAccent,
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    width: 100,
                    height: 50,
                    child: Text(
                      _counter.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                height: 50,
                child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        ++_counter;
                      });
                    },
                    child: Text(
                      "تسبيح",
                      style: TextStyle(
                        fontSize: 23,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                        primary: Colors.redAccent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ))),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _counter = 0;
                      });
                    },
                    child: Text(
                      "مسح",
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                    )),
              ),




            ],
          ),
        ),
      ),
    );
  }

  void _changeItem(String Item) {
    if (_alZeker != Item)
      setState(() {
        _alZeker = Item;
        _counter = 0;
      });
  }
}
