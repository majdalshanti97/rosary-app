import 'package:azkar_app/Screens/azkar_screen.dart';
import 'package:flutter/material.dart';


void main() => runApp(MainApp());


class MainApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AlAzkar(),
    );
  }
}


